import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignuppPage } from './signupp.page';

const routes: Routes = [
  {
    path: '',
    component: SignuppPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignuppPageRoutingModule {}
