import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignuppPageRoutingModule } from './signupp-routing.module';

import { SignuppPage } from './signupp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignuppPageRoutingModule
  ],
  declarations: [SignuppPage]
})
export class SignuppPageModule {}
